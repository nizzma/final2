import sys
from transforms import mirror, blur, grayscale
from images import read_img, write_img


def transformar(image, transformation):
    if transformation == 'mirror':
        return mirror(image)
    elif transformation == 'blur':
        return blur(image)
    elif transformation == 'grayscale':
        return grayscale(image)
    else:
        raise ValueError("Transformación no soportada.")


def main():
    if len(sys.argv) != 3:
        print("Uso: python transform_simple.py <archivo_imagen> <transformación>")
        return

    image_file = sys.argv[1]
    transformation = sys.argv[2]

    image = read_img(image_file)

    transformed_image = transformar(image, transformation)

    if '.png' in image_file:
        output_file = image_file.replace('.png', '_trans.png')
    if '.jpg' in image_file:
        output_file = image_file.replace('.jpg', '_trans.jpg')

    write_img(transformed_image, output_file)
    print(f"Imagen transformada guardada como {output_file}")


if __name__ == '__main__':
    main()
