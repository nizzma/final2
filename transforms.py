from images import create_blank

def mirror(image: dict) -> dict:
    width, height = image['width'], image['height']
    pixels = image['pixels']

    pixels_espejados = []
    for y in range(height):
        for x in range(width):
            pixels_espejados.append(pixels[y * width + (width - 1 - x)])

    return {
        'width': width,
        'height': height,
        'pixels': pixels_espejados
    }


def grayscale(image: dict) -> dict:
    width, height = image['width'], image['height']
    imagen_gris = create_blank(width, height)

    def rgb_a_gris(rgb):
        gris = sum(rgb) // 3
        return (gris, gris, gris)

    for i in range(width * height):
        imagen_gris['pixels'][i] = rgb_a_gris(image['pixels'][i])

    return imagen_gris


def blur(image: dict) -> dict:
    width, height = image['width'], image['height']
    imagen_difuminada = create_blank(width, height)

    def obtener_pixel(x, y):
        if 0 <= x < width and 0 <= y < height:
            return image['pixels'][x + width * y]
        return None

    for y in range(height):
        for x in range(width):
            vecinos = []
            for dx in (-1, 0, 1):
                for dy in (-1, 0, 1):
                    pixel = obtener_pixel(x + dx, y + dy)
                    if pixel is not None:
                        vecinos.append(pixel)
            r = sum(p[0] for p in vecinos) // len(vecinos)
            g = sum(p[1] for p in vecinos) // len(vecinos)
            b = sum(p[2] for p in vecinos) // len(vecinos)
            imagen_difuminada['pixels'][x + width * y] = (r, g, b)

    return imagen_difuminada


def change_colors(image: dict, original: list[tuple[int, int, int]], change: list[tuple[int, int, int]]) -> dict:
    mapa_colores = dict(zip(original, change))
    width, height = image['width'], image['height']
    imagen_cambiada = create_blank(width, height)

    for i in range(width * height):
        imagen_cambiada['pixels'][i] = mapa_colores.get(image['pixels'][i], image['pixels'][i])

    return imagen_cambiada


def rotate(image: dict, direction: str) -> dict:
    width, height = image['width'], image['height']

    if direction == 'right':
        imagen_rotada = create_blank(height, width)
        for y in range(height):
            for x in range(width):
                imagen_rotada['pixels'][height - 1 - y + height * x] = image['pixels'][x + width * y]
    elif direction == 'left':
        imagen_rotada = create_blank(height, width)
        for y in range(height):
            for x in range(width):
                imagen_rotada['pixels'][y + height * (width - 1 - x)] = image['pixels'][x + width * y]
    else:
        raise ValueError("Dirección inválida: use 'left' o 'right'")

    return imagen_rotada


def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    width, height = image['width'], image['height']
    nueva_anchura = width + abs(horizontal)
    nueva_altura = height + abs(vertical)
    imagen_desplazada = create_blank(nueva_anchura, nueva_altura)

    for y in range(height):
        for x in range(width):
            nuevo_x = x + horizontal
            nuevo_y = y + vertical
            if 0 <= nuevo_x < nueva_anchura and 0 <= nuevo_y < nueva_altura:
                imagen_desplazada['pixels'][nuevo_x + nueva_anchura * nuevo_y] = image['pixels'][x + width * y]

    return imagen_desplazada


def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    anchura_original = image['width']
    imagen_recortada = create_blank(width, height)

    for j in range(height):
        for i in range(width):
            imagen_recortada['pixels'][i + width * j] = image['pixels'][(x + i) + anchura_original * (y + j)]

    return imagen_recortada


def filter(image: dict, r: float, g: float, b: float) -> dict:
    width, height = image['width'], image['height']
    imagen_filtrada = create_blank(width, height)

    def aplicar_filtro(rgb):
        return (
            min(int(rgb[0] * r), 255),
            min(int(rgb[1] * g), 255),
            min(int(rgb[2] * b), 255)
        )

    for i in range(width * height):
        imagen_filtrada['pixels'][i] = aplicar_filtro(image['pixels'][i])

    return imagen_filtrada
