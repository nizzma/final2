import sys
from transforms import change_colors, rotate, shift, crop, filter, mirror, blur, grayscale
from images import read_img, write_img


def separar_rgb(rgb_list_str):
    rgb_list = rgb_list_str.split(':')
    result = []
    for rgb in rgb_list:
        rgb_tuple = tuple(map(int, rgb.split(',')))
        result.append(rgb_tuple)
    return result


def transformar(image, transformation, args):
    if transformation == 'change_colors':
        original_colors = separar_rgb(args[0])
        change_colors_list = separar_rgb(args[1])
        return change_colors(image, original_colors, change_colors_list)
    elif transformation == 'rotate':
        return rotate(image, args[0])
    elif transformation == 'shift':
        horizontal = int(args[0])
        vertical = int(args[1])
        return shift(image, horizontal, vertical)
    elif transformation == 'crop':
        x = int(args[0])
        y = int(args[1])
        width = int(args[2])
        height = int(args[3])
        return crop(image, x, y, width, height)
    elif transformation == 'filter':
        r = float(args[0])
        g = float(args[1])
        b = float(args[2])
        return filter(image, r, g, b)
    elif transformation == 'mirror':
        return mirror(image)
    elif transformation == 'blur':
        return blur(image)
    elif transformation == 'grayscale':
        return grayscale(image)
    else:
        raise ValueError("Transformación no soportada.")


def main():
    if len(sys.argv) < 3:
        print("Uso: python transform_multi.py <archivo_imagen> <transformación> [args...] ...")
        return

    image_file = sys.argv[1]
    transformations = sys.argv[2:]

    image = read_img(image_file)

    i = 0
    while i < len(transformations):
        transformation = transformations[i]
        args = []

        if transformation == 'change_colors':
            args = transformations[i + 1:i + 3]
            i += 3
        elif transformation == 'rotate':
            args = [transformations[i + 1]]
            i += 2
        elif transformation == 'shift':
            args = transformations[i + 1:i + 3]
            i += 3
        elif transformation == 'crop':
            args = transformations[i + 1:i + 5]
            i += 5
        elif transformation == 'filter':
            args = transformations[i + 1:i + 4]
            i += 4
        else:
            i += 1

        image = transformar(image, transformation, args)

    if '.png' in image_file:
        output_file = image_file.replace('.png', '_trans.png')
    if '.jpg' in image_file:
        output_file = image_file.replace('.jpg', '_trans.jpg')

    write_img(image, output_file)
    print(f"Imagen transformada guardada como {output_file}")


if __name__ == '__main__':
    main()
